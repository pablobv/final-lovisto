# Final Lo Visto

Práctica final del curso 2020/21

* Nombre:Pablo Barquero Villanueva
* Titulación:Grado en Ingeniería en Tecnologías de Telecomunicación
* Despliegue (url):http://pablobv.pythonanywhere.com/LoVisto/
* Video básico (url):https://youtu.be/cLGAiXaXVGg

* Cuenta Admin Site*
admin/admin
pablobv/admin2 

* Resumen parte obligatoria:
  Funcionamiento básico: En la parte superior, podemos observar las siguientes opciones:
 	-Home: Home es la página de inicio, en ella podemos encontrar las últimas 10 aportaciones del sitio.
	-Aportaciones: Esta es la página donde quedan reflejadas todas las aportaciones que se han hecho. Si seleccionamos alguna de ellas, 		podremos ver una breve descripción de la misma, además de todos los comentarios que tenga.
	-Info: Es esta página, contiene información acerca de la página web.
	-XML: Descargar como fichero XML.
	-JSON: Descargar como fichero JSON.
	-Una vez que iniciamos sesión con uno de los usuarios registrados aparece un campo User, donde se reflejan las aportaciones y 		comentarios que ha hecho ese usuario.

 
* Lista partes opcionales*

* Nombre parte:favicon
* Nombre parte:Reconocen recursos de los 4 sitios propuestos: youtube, reddit, wikipedia y la AEMET.
