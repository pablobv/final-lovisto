#!/usr/bin/python3
from bs4 import BeautifulSoup
import urllib.request


def html_parser(url):
    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')

    # Get meta property='og:title'
    title = soup.find('meta', property='og:title')
    if title:
        title = title["content"]
    else:
        title = soup.title.string

    # Get meta property='og:image'
    ogImage = soup.find('meta', property='og:image')
    if ogImage:
        ogImage = ogImage["content"]

    return title, ogImage


def info_html(url):
    (title, image) = html_parser(url)
    html = '<div class ="og"><p>' + title + '</p>'

    if image:
        html = html + '<img src = "'+image+'" >'

    html = html + '</div>'

    return html
