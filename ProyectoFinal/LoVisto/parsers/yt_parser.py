#!/usr/bin/python3
import json
import urllib.request


def yt_parser(site):
    #Obtengo video de /watch?v={video}
    #URL: https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v={video}
    path = site.split("=")
    url = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v="+path[-1]

    response = urllib.request.urlopen(url)
    jsonStream = json.loads(response.read())
    return jsonStream['title'], jsonStream['author_name'], jsonStream['html']


def info_yt(url):
    (title, author, iframe) = yt_parser(url)
    html = '<div class="youtube">\
<p>Video YouTube: '+title+'</p>' + iframe + '\
<p>Autor: ' + author + '.</p>\
<a href="'+url+'">Video en YouTube</a></p>\
</div>'

    return html
