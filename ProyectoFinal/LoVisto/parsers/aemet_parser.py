#!/usr/bin/python3
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib.request


def crear_matriz(filas, columnas):
    matriz = []
    for i in range(filas):
        a = [""] * columnas
        matriz.append(a)
    return matriz


prediccion = crear_matriz(10, 7)
datos = ['', '', '', 0]


class Handler(ContentHandler):
    def __init__(self):
        datos[0] = ''
        datos[1] = ''
        datos[2] = ''
        datos[3] = 0
        self.isMunicipio = False
        self.isProvincia = False
        self.isCopyright = False
        self.tipoPrediccion = ""
        self.minOMax = "MIN"

    def startElement(self, name, attrs):
        if name == 'nombre':
            self.isMunicipio = True
        elif name == 'provincia':
            self.isProvincia = True
        elif name == 'copyright':
            self.isCopyright = True
        elif name == 'dia':
            prediccion[datos[3]][0] = attrs.get('fecha')
        elif name == 'temperatura':
            self.tipoPrediccion = "TEMPERATURA"
        elif name == 'sens_termica':
            self.tipoPrediccion = "SENSACION"
        elif name == 'humedad_relativa':
            self.tipoPrediccion = "HUMEDAD"
        elif name == 'maxima':
            self.minOMax = "MAX"
        elif name == 'minima':
            self.minOMax = "MIN"

    def endElement(self, name):
        if name == 'nombre':
            self.isMunicipio = False
            self.municipio = ""
        elif name == 'provincia':
            self.isProvincia = False
            self.provincia = ""
        elif name == 'copyright':
            self.isCopyright = False
            self.copyright = ""

        elif name == 'dia':
            self.tipoPrediccion = ""
            datos[3] = datos[3] + 1
        elif name == 'sens_termica' or name == 'humedad_relativa' or name == 'temperatura' or name == "minima":
            self.tipoPrediccion = ""

    def characters(self, chars):
        if chars != '\n':
            if self.isMunicipio:
                datos[0] = datos[0] + chars
            elif self.isProvincia:
                datos[1] = datos[1] + chars
            elif self.isCopyright:
                datos[2] = datos[2] + chars

            elif self.tipoPrediccion == "TEMPERATURA" and self.minOMax == "MAX":
                prediccion[datos[3]][1] = prediccion[datos[3]][1] + chars
            elif self.tipoPrediccion == "TEMPERATURA" and self.minOMax == "MIN":
                prediccion[datos[3]][2] = prediccion[datos[3]][2] + chars

            elif self.tipoPrediccion == "SENSACION" and self.minOMax == "MAX":
                prediccion[datos[3]][3] = prediccion[datos[3]][3] + chars
            elif self.tipoPrediccion == "SENSACION" and self.minOMax == "MIN":
                prediccion[datos[3]][4] = prediccion[datos[3]][4] + chars

            elif self.tipoPrediccion == "HUMEDAD" and self.minOMax == "MAX":
                prediccion[datos[3]][5] = prediccion[datos[3]][5] + chars
            elif self.tipoPrediccion == "HUMEDAD" and self.minOMax == "MIN":
                prediccion[datos[3]][6] = prediccion[datos[3]][6] + chars


def aemet_parser(site):
    path = site.split("d")
    url = "https://www.aemet.es/xml/municipios/localidad_"+path[-1]+".xml"
    xmlStream = urllib.request.urlopen(url)

    Parser = make_parser()
    Parser.setContentHandler(Handler())
    Parser.parse(xmlStream)

    return datos[0], datos[1], datos[2], prediccion[:datos[3]][:]


def info_aemet(url):
    (municipio, provincia, copyright, prediccion) = aemet_parser(url)

    html = '<div class="aemet">\
<p>Datos AEMET para '+municipio+' ('+provincia+')</p>\
<ul>'
    for p in prediccion:
        html = html + '<li>'+p[0]+'. Temperatura: '+p[1]+'/'+p[2]+', sensación: '+p[3]+'/'+p[4]+', humedad: '+p[5]+'/'+p[6]+'.</li>'
    html = html + '\
</ul>\
<p>'+copyright+'</p>.\
<p><a href="'+url+'">Página original en AEMET</a></p>\
</div>'
    return html
