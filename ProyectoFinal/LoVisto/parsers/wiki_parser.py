#!/usr/bin/python3
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib.request
import json

datos = ['', '']


class Handler(ContentHandler):
    def __init__(self):
        self.isExtract = False
        self.content = ""

    def startElement(self, name, attrs):
        if name == 'extract':
            self.isExtract = True
        if name == 'page':
            datos[1] = attrs.get('pageid')

    def endElement(self, name):
        if name == 'extract':
            self.isExtract = False
            datos[0] = self.content
            self.content = ""

    def characters(self, chars):
        if self.isExtract:
            self.content = self.content + chars


def wiki_parser(site):
    # Obtengo articulo de /wiki/{articulo}
    path = site.split("/")
    site = path[-1]

    # Primero hago un parser XML para obtener 400 caracteres de texto
    url_texto = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="+site+"&prop=extracts&exintro&explaintext"
    xmlStream = urllib.request.urlopen(url_texto)

    Parser = make_parser()
    Parser.setContentHandler(Handler())
    Parser.parse(xmlStream)
    texto = datos[0]

    # Ahora hago un parser JSON para obtener la imagen
    url_imagen = "https://es.wikipedia.org/w/api.php?action=query&titles="+site+"&prop=pageimages&format=json&pithumbsize=100"
    response = urllib.request.urlopen(url_imagen)
    jsonStream = json.loads(response.read())

    return texto[0:399], jsonStream['query']["pages"][datos[1]]["thumbnail"]["source"], 2


def info_wiki(url):
    (texto, imagen, copyright) = wiki_parser(url)
    html = '<div class="wikipedia">\
<p>Artı́culo Wikipedia: '+texto+'</p>\
<p><img src="'+imagen+'"></p>\
<p><a href="'+url+'">Artı́culo original</a>.</p>\
</div>'

    return html
