from urllib.parse import urlparse
from bs4 import BeautifulSoup
import urllib.request

from .html_parser import info_html
from .yt_parser import info_yt
from .aemet_parser import info_aemet
from .wiki_parser import info_wiki


def isaemet(site, path):
    if site == "www.aemet.es" or site == "aemet.es":
        if path.startswith("/es/eltiempo/prediccion/municipios/"):
            if path.split("d")[-1].isdigit():
                return True


def isyt(site, path):
    if site == "www.youtube.com" or site == "youtube.com":
        if path.startswith("/watch"):
            return True


def iswiki(site, path):
    if site == "es.wikipedia.org":
        if path.startswith("/wiki/"):
            return True


def get_info_from_url(url):
    site = urlparse(url).netloc
    path = urlparse(url).path
    print("Site: " + site + ", path: " + path)

    if isaemet(site, path):
        return info_aemet(url)
    if isyt(site, path):
        return info_yt(url)
    if iswiki(site, path):
        return info_wiki(url)

    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')
    if soup:
        return info_html(url)
    else:
        return '<div class="html"> <p>Información extendida no disponible</p> </div>'
