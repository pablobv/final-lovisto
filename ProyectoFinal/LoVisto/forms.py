from django import forms
from .models import Contenidos, Comentario


class ContenidoForm(forms.ModelForm):
    class Meta:
        model = Contenidos
        fields = ('titulo', 'url', 'descripcion')


class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ('cuerpo',)
