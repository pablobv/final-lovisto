from django.db import models

# Create your models here.


class Contenidos (models.Model):
    titulo = models.CharField(max_length=256)
    descripcion = models.TextField()
    usuario = models.TextField()
    url = models.TextField()
    votos_positivos = models.IntegerField()
    votos_negativos = models.IntegerField()
    fecha = models.DateTimeField()
    info = models.TextField()

    def __str__(self):
        return str(self.id) + ": " + str(self.titulo)


class Comentario (models.Model):
    contenido = models.ForeignKey(Contenidos, on_delete=models.CASCADE)
    cuerpo = models.TextField(default="No body")
    fecha = models.DateTimeField()
    usuario = models.TextField()

    def __str__(self):
        return str(self.id) + ": " + self.titulo


class Voto (models.Model):
    TIPOS = [
        ('+', 'positivo'),
        ('-', 'negativo')
    ]
    contenido = models.ForeignKey(Contenidos, on_delete=models.CASCADE)
    usuario = models.TextField()
    tipo = models.CharField(max_length=1, choices=TIPOS, default="Null vote")

    def __str__(self):
        return str(self.id) + ": " + str(self.tipo)
