from django.contrib import admin
from LoVisto.models import Contenidos, Comentario, Voto

# Register your models here.

admin.site.register(Contenidos),
admin.site.register(Comentario),
admin.site.register(Voto),
