from django.urls import path
from . import views

urlpatterns = [
    path('', views.home),
    path('info/', views.info),
    path('user/', views.user),
    path('aportaciones/', views.aportaciones),
    path('aportaciones/<int:id>', views.show_content)
]
